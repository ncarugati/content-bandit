import os
import json
import random
import praw
import requests
import hashlib

class Site:
    def __init__(self):
        with open('config.json') as file:
            self.config = json.load(file)

    def crawl(self):
        pass

    def extract(self):
        pass

class Reddit(Site):

    SUBREDDIT_CRAWL_LIST = ['dankmemes']

    ENDPOINT_PREFIX = 'https://reddit.com/r/'

    def __init__(self):
        Site.__init__(self)

    def get_image(self):
        url_list = self.crawl()
        return self.extract(url_list)

    def crawl(self):
        Site.crawl(self)
        reddit = praw.Reddit(client_id=self.config['reddit']['client_id'],
                     client_secret=self.config['reddit']['client_secret'],
                     password=self.config['reddit']['password'],
                     user_agent=self.config['reddit']['user_agent'],
                     username=self.config['reddit']['username'])
        url_list = []
        url_prefix = 'https://i.redd.it/'
        for sreddit in self.SUBREDDIT_CRAWL_LIST:
            subreddit = reddit.subreddit(sreddit).top('all')
            for submission in subreddit:
                if submission.url.startswith(url_prefix):
                    url_list.append(submission.url)
        return url_list

    def extract(self, url_list):
        Site.extract(self)
        if not(isinstance(url_list, list)):
            raise Exception('Content to be extracted must be contained within a list')

        url_to_extract = random.choice(url_list)
        hasher = hashlib.md5()
        hasher.update(url_to_extract.encode('utf-8'))
        req = requests.get(url_to_extract)
        file_name = hasher.hexdigest() + '.png'
        if req.status_code == 200:
            with open("./output/" + file_name, 'wb') as f:
                f.write(req.content)
            return file_name
        return None

class Imgur(Site):

    TAG_CRAWL_LIST = ['dankmemes']

    def __init__(self):
        Site.__init__(self)
        print()
        self.url_base = ' https://api.imgur.com/3/gallery/t/'
        self.headers = {'Authorization': 'Client-ID ' + self.config['imgur']['client_id']}

    def get_image(self):
        url_list = self.crawl()
        return self.extract(url_list)

    def crawl(self):
        Site.crawl(self)
        global reddit
        url_list = []
        url_prefix = 'https://i.imgur.com/'
        for tag in self.TAG_CRAWL_LIST:
            endpoint = self.url_base + tag + '/hot/viral'
            tag_payload = requests.get(endpoint, headers=self.headers)
            data = tag_payload.json()
            for entry in data['data']['items']:
                if 'images' in entry:
                    url_list.append(entry['images'][0]['link'])
        return url_list

    def extract(self, url_list):
        Site.extract(self)
        if not(isinstance(url_list, list)):
            raise Exception('Content to be extracted must be contained within a list')

        url_to_extract = random.choice(url_list)
        hasher = hashlib.md5()
        hasher.update(url_to_extract.encode('utf-8'))
        req = requests.get(url_to_extract)
        file_name = hasher.hexdigest() + '.png'
        if req.status_code == 200:
            with open("./output/" + file_name, 'wb') as f:
                f.write(req.content)
            return file_name
