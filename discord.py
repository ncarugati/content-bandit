import discord
from discord.ext import commands
import asyncio

class DiscordBot():

    def __init__(self):
        description = 'The content-bandit Discord extension'
        client = commands.Bot(command_prefix='$', description=description)
        client.run('token')

    @bot.event
    async def on_ready():
        print('Logged in as')
        print(client.user.name)
        print(client.user.id)
        print('------')


    @bot.command()
    async def post(url : str):
        await bot.say(url)

    @bot.command()
    async def get():
        await bot.say('Retrieve Placeholder')
