import sys
import time
import random

from page import Page
from website import Site
from website import Reddit
from website import Imgur
from threading import Thread


def loader():
    print("[BANDIT]: Bandit Activated Searching for content...")

    imgur = Imgur()
    reddit = Reddit()
    website_selection = [imgur, reddit]

    instance = random.choice(website_selection)

    p = Page()
    p.post(instance.get_image())
    exit()


if __name__ == "__main__":
    loader()
