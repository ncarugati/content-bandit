import facebook
import json

class Page():

    config = None

    def __init__(self):
        with open('config.json') as file:
            self.config = json.load(file)

    def post(self, file_name):
        access_token = self.config['facebook_page']['page_access_token']
        graph = facebook.GraphAPI(access_token=access_token, version='3.1')
        graph.put_photo(image=open('./output/' + file_name, 'rb'))
        print('Image posted successfully');
